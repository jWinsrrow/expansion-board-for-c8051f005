# Expansion board for C8051F005

Exapansion borad for the C8051F005.


## Project stage

**Ready for manufacturing**

## Project status

* [X] Minimal component schematics.
* [X] Minimal component schematics review.
* [X] Board component schematics.
* [X] Board component schematics review.
* [X] Component selection.
* [X] Component selection alternative.
* [X] Footprint association.
* [X] Footprint review.
* [X] PCB Rules setup.
* [X] PCB Rules setup review.
* [X] Component placing.
* [X] Component placing review.
* [X] PCB rough routing.
* [X] PCB fine routing. 
* [X] PCB layout review. 
* [X] PCB DRC. 
* [ ] Gerbers. 
* [ ] Gerbers review.
* [ ] Hardware production.
* [ ] Hardware tested.
